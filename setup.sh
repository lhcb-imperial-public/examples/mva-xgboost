#This sets up a nice environment on the lxplus-like machines
if [ -f /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh ]; then
  source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh
fi

export QT_QPA_PLATFORM=offscreen 

if [ ! -d mvaenv ]; then
  echo "################################################################################"
  echo "#                                                                              #"
  echo "#                   Making a new virtual python environment                    #"
  echo "#                                                                              #"
  echo "################################################################################"

  python3 -m venv mvaenv
   export PYTHONPATH="$(pwd)/python/:$(pwd)/mvaenv/lib/python3.9/site-packages/:${PYTHONPATH}"

  . mvaenv/bin/activate

  echo "################################################################################"
  echo "#                                                                              #"
  echo "#                       Installing the required packages                       #"
  echo "#                                                                              #"
  echo "################################################################################"

  pip3 install --upgrade xgboost



else
   export PYTHONPATH="$(pwd)/python/:$(pwd)/mvaenv/lib/python3.9/site-packages/:${PYTHONPATH}"
  . mvaenv/bin/activate
fi

