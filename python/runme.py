import xgboost
import numpy

from ROOT import *

###############################################################################
#  Prepare the datasets                                                       #
###############################################################################

#In this simple example we have alread got a file containing the simulation to
#use as a signal proxy, and the upper mass side band from data to use as a
#background proxy
sigfilename = 'data/B2JPsiKst_MC.root'
bkgfilename = 'data/B2JPsiKst_sideband.root'

#This will read the required features from the file
def getFromFile(filename, treename, variables, selection = None):
  f = TFile(filename)
  t = f.Get(treename)

  data = []

  for ievent in range(t.GetEntries()):
    t.GetEntry(ievent)
    if selection and not selection(tree):
      continue
    
    row = [getattr(t, v) for v in variables]
    data.append(row)
    
  return data

#This will split the data into a training and testing dataset
def split(data, frac, label):
  Ntrain = int(frac*len(data))
  data_train = numpy.array(data[:Ntrain])
  data_test  = numpy.array(data[Ntrain:])
  label_train = numpy.array([label]*len(data_train))
  label_test  = numpy.array([label]*len(data_test))
  return {'data_train':data_train, 'data_test':data_test, 'label_train':label_train, 'label_test':label_test}



#These will be the features we ill use in this example
#            0         1                    2                 3                 4             5        6       7           8
features = ['B_PT', 'B_ENDVERTEX_CHI2', 'B_IPCHI2_OWNPV', 'B_FDCHI2_OWNPV', 'B_DIRA_OWNPV', 'K_PT', 'pi_PT', 'muplus_PT', 'muminus_PT']

#Get the signal and background data
sig_frame = getFromFile(sigfilename, 'DecayTree', features)
bkg_frame = getFromFile(bkgfilename, 'DecayTree', features)

#Make training and testing data sets, 90% training 10% testing
sigsamples = split(sig_frame, 0.9, 1)
bkgsamples = split(bkg_frame, 0.9, 0)

x_train = numpy.concatenate((sigsamples['data_train'], bkgsamples['data_train']))
x_test  = numpy.concatenate((sigsamples['data_test'], bkgsamples['data_test']))
y_train = numpy.concatenate((sigsamples['label_train'], bkgsamples['label_train']))
y_test  = numpy.concatenate((sigsamples['label_test'], bkgsamples['label_test']))


###############################################################################
#  Optimise hyperparameters                                                   #
###############################################################################

#We're going to look at optimising the learning rate and the maximum tree
#depth. We're not going to do this in a particularly clever way. We will find
#A rough idea of the best depth, then optimise the learning rate with that
#depth, and with this learning rate reoptimise the rate. A more thorough way
#would be to grid search the parameter space, but that will take a while...

Retrain_MVA = True
if Retrain_MVA:

  #First find a decent depth for a high learning rate
  learning_rate = 0.3
  max_depth = 6
  score = 0
  best_depth = max_depth
  best_model = None
  for i in range(3, 11):
    max_depth = i
    model = xgboost.XGBClassifier(use_label_encoder=False, eval_metric='auc', early_stopping_rounds=50, learning_rate=learning_rate,max_depth=max_depth,n_estimators=1000)
    model.fit(x_train, y_train, eval_set=[(x_test,y_test)])
    if model.best_score > score:
      best_depth = max_depth
      score = model.best_score

  #Now optimise the learning rate
  best_lr = 0.3
  score = 0
  for i in range(30):
    max_depth = best_depth
    learning_rate = 0.01 + 0.01*i
    model = xgboost.XGBClassifier(use_label_encoder=False, eval_metric='auc', early_stopping_rounds=50, learning_rate=learning_rate,max_depth=max_depth,n_estimators=1000)
    model.fit(x_train, y_train, eval_set=[(x_test,y_test)])
    if model.best_score > score:
      best_lr = learning_rate
      score = model.best_score

  #Reoptimise the depth for this learning_rate
  score = 0
  best_depth = max_depth
  for i in range(3, 11):
    max_depth = i
    learning_rate = best_lr
    model = xgboost.XGBClassifier(use_label_encoder=False, eval_metric='auc', early_stopping_rounds=50, learning_rate=learning_rate,max_depth=max_depth,n_estimators=1000)
    model.fit(x_train, y_train, eval_set=[(x_test,y_test)])
    if model.best_score > score:
      best_depth = max_depth
      score = model.best_score
      best_model = model
  print(score, best_depth, best_lr)

  #saving the best model, so we won't have to do the training again unless we want to make a change
  model.save_model('model.json')

model = xgboost.XGBClassifier()
model.load_model('model.json')

###############################################################################
#  Some diagnostic plots                                                      #
###############################################################################

#Make feature importance plot
import matplotlib
import matplotlib.pyplot as plt
a = xgboost.plot_importance(model)
plt.savefig("feature_importance.pdf")

#Make output distributions plot
htest_sig = TH1D('htest_sig', 'Signal (test sample)', 100, 0, 1)
htest_bkg = TH1D('htest_bkg', 'Background (test sample)', 100, 0, 1)
htrain_sig = TH1D('htrain_sig', 'Signal (training sample)', 100, 0, 1)
htrain_bkg = TH1D('htrain_bkg', 'Background (training sample)', 100, 0, 1)

def fillHistos(model, x, y, h):
  probability = model.predict_proba(x)
  for p,which in zip(probability, y):
    h[which].Fill(p[1])

fillHistos(model, x_train, y_train, [htrain_bkg, htrain_sig])
fillHistos(model, x_test,  y_test,  [htest_bkg,  htest_sig])

htrain_bkg.Scale(1./htrain_bkg.GetEntries())
htrain_sig.Scale(1./htrain_sig.GetEntries())
htest_bkg.Scale(1./htest_bkg.GetEntries())
htest_sig.Scale(1./htest_sig.GetEntries())

htest_sig.SetLineColor(kBlue)
htrain_sig.SetLineColor(kBlue)
htest_sig.SetFillColor(kAzure-5)

htrain_bkg.SetLineColor(kRed)
htest_bkg.SetFillColor(kRed)
htest_bkg.SetFillStyle(3004)

htrain_bkg.SetMarkerStyle(20)
htrain_bkg.SetMarkerColor(kRed)
htrain_sig.SetMarkerStyle(20)
htrain_sig.SetMarkerColor(kBlue)

c = TCanvas()
htest_sig.SetStats(False)
htest_sig.Draw('hist')
htest_bkg.Draw('hist same')
htrain_bkg.Draw('same e1')
htrain_sig.Draw('same e1')

maxh = max(htrain_sig.GetMaximum(), htest_sig.GetMaximum(), htrain_bkg.GetMaximum(), htest_bkg.GetMaximum())

htest_sig.GetYaxis().SetRangeUser(0, 1.2*maxh)
c.BuildLegend(0.3, 0.6, 0.9, 0.9)
c.Update()
c.Modified()
###############################################################################
#  Optimising the working point                                               #
###############################################################################
#Our MVA will tell us the probability of an event being signal like, but what
#range of probabilities should we include in our analysis? We need to optimise
#this range to suit our analysis, the most important thing is to pick a figure-
#of-merit. There are a number in the literature:-
#Useful reading: arXiv:physics/0308063
#This advocates the so-called Punzi figure of merit, efficiency/(a/2 + sqrt(B)),
#and a lot of people use S/sqrt(S+B). S is the expected signal, B is the
#expected background, and a is the number of sigma significance you'd like from
#the measurement. The punzi figure of merit has the advantage that it doesn't
#depend on having knowledge of the signal rate, which is useful if that's what
#you're trying to make a measurement of. Personally, I like to optimise based
#on the result of the significance using fits toys with Wilks' theorem, but
#that relies on being able to generate realistic toys (not covered here).


#precompute test results
prob_test = model.predict_proba(x_test)

#This will compute the efficiencies for a specific cut on the signal probability
#and estimate the uncertainy in that efficiency
def computeEfficiencies(cutvalue):
  pass_cut = [0,0]
  total = [0,0]
  for p,which in zip(prob_test,y_test):
    total[which] += 1
    if p[1] >= cutvalue:
      pass_cut[which] += 1

  return ((pass_cut[0]/float(total[0]), 1.0/total[0]*sqrt(pass_cut[0]*(1-pass_cut[0]/total[0]))),(pass_cut[1]/float(total[1]), 1.0/total[1]*sqrt(pass_cut[1]*(1-pass_cut[1]/total[1]))))

from math import sqrt

#This will compute the punzi Figure-of-merit for a 3 sigma result
def FoMPunzi(cutvalue, B_before_cut):
  eff = computeEfficiencies(cutvalue)
  a = 0.5*3
  B = B_before_cut*eff[0][0]
  sqrtB = sqrt(B)

  fom = eff[1][0]/(a + sqrtB)
  if sqrtB > 0:
    err = sqrt((eff[1][1]/(a+sqrtB))**2 + (0.5*B_before_cut*eff[0][1]*eff[1][0]/((a+sqrtB)**2*sqrtB))**2)
  else:
    err = sqrt((eff[1][1]/(a+sqrtB))**2 + (0.5*B_before_cut*eff[0][1]*eff[1][0]/((a+sqrtB)**2*0.1))**2)

  return (fom,err)

#This will compute the S/sqrt(S+B) Figure-of-merit
def FoMSonSpB(cutvalue, B_before_cut, S_before_cut):
  eff = computeEfficiencies(cutvalue)
  B = B_before_cut*eff[0][0]
  S = S_before_cut*eff[1][0]

  sB = B_before_cut*eff[0][1]
  sS = S_before_cut*eff[1][1]

  if S == 0 and B == 0:
    return (0,0)

  fom = S/sqrt(S + B)
  err = sqrt(sS**2*((S+B)**-0.5 - 0.5*S/(S+B)**1.5)**2 + sB**2*(0.5*S/(S+B)**1.5)**2)
  return (fom,err)
    

#How much background in the fit window? This we can make a reliable estimate of
#if we're using the upper mass sideband of the data as a background proxy, then
#the we can use that to extrapolate to our fit window. Here we will treat the
#background as distributed exponentially, and the exponent take from a fit to
#the upper mass sideband

# Start with the data in sideband
B = len(bkg_frame)
#Then scaling from 5500--5700 region to 5170--5700 full fit window
B *= (exp(-2.23489*5.17) - exp(-2.23489*5.7))/(exp(-2.23489*5.5) - exp(-2.23489*5.7))

#How much signal should we expect? For assume we know we expect 100 events
S = 100

#We're going to plot both figures of merit as a function of the cut value
mg = TMultiGraph()
gp = TGraphErrors()
gs = TGraphErrors()
for i in range(500):
  cut = i*0.002
  fp = FoMPunzi(cut, B)
  fs = FoMSonSpB(cut, B, S)
  gp.SetPoint(i, cut, fp[0])
  gp.SetPointError(i, 0, fp[1])
  gs.SetPoint(i, cut, fs[0])
  gs.SetPointError(i, 0, fs[1])

gp.SetLineColor(kBlack)
gs.SetLineColor(kRed)
gp.SetTitle('#varepsilon/(1.5 + #sqrt{B})')
gs.SetTitle('S/#sqrt{S+B}')
mg.Add(gp)
mg.Add(gs)
mg.GetYaxis().SetTitle('FoM')
mg.GetXaxis().SetTitle('MVA selection')
c2 = TCanvas()
mg.Draw('AL')
c2.BuildLegend()
c2.Update()
c2.Modified()

#From this we probably want to select events with a probablilty of > 0.95.
#Notice that the FoM gets increasingly unstable as you approach 1. This is due
#to the finite sample size and the low survial rates of events in both the
#signal and background samples so +/-1 background or signal event makes a large
#change to the efficiency (also the binomial approximation in the uncertainty)

###############################################################################
#  Writing the output of the MVA                                              #
###############################################################################
#Here we will write a new tree (not very efficient, would be better to make a
#friend to the existing tree), that contains the output of the MVA in a branch
#called MVA.

from TreeMaker import TreeMaker
from TreeWrapper import TreeWrapper

with TreeMaker("B2JPsiKst_data_withBDT.root", "DecayTree", ["B_M", "B_PT", "B_ENDVERTEX_CHI2", "B_IPCHI2_OWNPV", "B_FDCHI2_OWNPV", "B_DIRA_OWNPV", "Kst_M", "JPsi_M", "K_PT", "pi_PT", "muplus_PT", "muminus_PT", "MVA"]) as outtree:
  tree = TreeWrapper("data/B2JPsiKst_data.root", "DecayTree")
  for entry in tree.entry():
    #give the model the data it needs
    row = [getattr(tree, v) for v in features]
    MVA = model.predict_proba([row])[0][1]


    outtree.Fill({
                   "B_M":tree.B_M,
                   "B_PT":tree.B_PT,
                   "B_ENDVERTEX_CHI2":tree.B_ENDVERTEX_CHI2,
                   "B_IPCHI2_OWNPV":tree.B_IPCHI2_OWNPV,
                   "B_FDCHI2_OWNPV":tree.B_FDCHI2_OWNPV,
                   "B_DIRA_OWNPV":tree.B_DIRA_OWNPV,
                   "Kst_M":tree.Kst_M,
                   "JPsi_M":tree.JPsi_M,
                   "K_PT":tree.K_PT,
                   "pi_PT":tree.pi_PT,
                   "muplus_PT":tree.muplus_PT,
                   "muminus_PT":tree.muminus_PT,
                   "MVA":MVA #the output of the classifier
                 })


###############################################################################
#  Drawing the output                                                         #
###############################################################################
#Here we will just draw the B mass distribution with and without the selection
#applied to the MVA for comparison.

c3 = TCanvas()
tree = TreeWrapper("B2JPsiKst_data_withBDT.root", "DecayTree")
tree.Draw("B_M", "MVA > -1",  "")       # no BDT cut
tree.Draw("B_M", "MVA >  0.5",  "same") # A loose BDT cut
tree.Draw("B_M", "MVA > 0.95", "same")  # The optimial cut
c3.Modified()
c3.Update()
input("Press ENTER to exit")



