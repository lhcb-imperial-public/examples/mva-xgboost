# MVA-xgboost

A tutorial for simple application of an MVA using XGBoost. The script trains a BDT, optimises some hyperparameters, makes some diagnostic plots, finds an appropriate optimal cut, applies the BDT to some sample data, and plots the data with a few example BDT cuts applied. See the annotated code in python/runme.py

## Running

  * source setup.sh
  * python python/runme.py

